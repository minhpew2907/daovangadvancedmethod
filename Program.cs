﻿const int SO_QUANG_VANG_THU_NHAT = 10;
const int GIA_QUANG_VANG_THU_NHAT = 10;
const int SO_QUANG_VANG_THU_HAI = 5;
const int GIA_QUANG_VANG_THU_HAI = 5;
const int SO_QUANG_VANG_THU_BA = 3;
const int GIA_QUANG_VANG_THU_BA = 2;
const int GIA_QUANG_VANG_CON_LAI = 1;


int choice = 0;
do
{
    Menu();
    choice = int.Parse(Console.ReadLine());
    Console.Clear();

    switch (choice)
    {
        case 1:
            Console.WriteLine("Nhap so quang vang ban thu duoc: ");
            int vangNhapVao = int.Parse(Console.ReadLine());

            int tienNhanDuoc = DaoVang(vangNhapVao);
            Console.WriteLine($"Ban nhan duoc tong {tienNhanDuoc} tien.");
            break;
        case 2:
            Console.WriteLine("Cam on ban da tham gia tro choi");
            Environment.Exit(0);
            break;
        default:
            Console.WriteLine("Nhap lua chon bang tren");
            break;
    }

} while (choice != 2);


int DaoVang(int vangNhapVao)
{
    int vang1 = Math.Min(vangNhapVao, SO_QUANG_VANG_THU_NHAT);
    int gia10QuangVang = vang1 * GIA_QUANG_VANG_THU_NHAT;
    vangNhapVao -= vang1;
    Console.WriteLine($"Nguoi choi nhan duoc {vang1} tien dau tien.");

    int vang2 = Math.Min(vangNhapVao, SO_QUANG_VANG_THU_HAI);
    int gia5QuangVang = vang2 * GIA_QUANG_VANG_THU_HAI;
    vangNhapVao -= vang2;
    Console.WriteLine($"Nguoi choi nhan duoc {vang2} tien thu hai.");

    int vang3 = Math.Min(vangNhapVao, SO_QUANG_VANG_THU_BA);
    int gia3QuangVang = vang3 * GIA_QUANG_VANG_THU_BA;
    vangNhapVao -= vang3;
    Console.WriteLine($"Nguoi choi nhan duoc {vang3} tien thu ba.");

    int giaQuangVangConLai = vangNhapVao * GIA_QUANG_VANG_CON_LAI;
    Console.WriteLine($"Nguoi choi nhan duoc {vangNhapVao} tien con lai.");

    int tongGiatien = (gia10QuangVang + gia5QuangVang + gia3QuangVang + giaQuangVangConLai);
    return tongGiatien;
}

void Menu()
{
    Console.WriteLine("----Tro choi dao vang 2.0----");
    Console.WriteLine("1. Choi");
    Console.WriteLine("2. Thoat");
    Console.WriteLine("-------------------");
    Console.WriteLine("Nhap lua chon: ");
}



